// const BASE_URL = `https://bahmni-apis-default-rtdb.firebaseio.com/`;
const BASE_URL = `http://localhost:8080`;

export const CONCEPT = `${BASE_URL}/concept`;
export const CONCEPT_NAME = `${BASE_URL}/conceptname`;
export const CONCEPT_CLASS = `${BASE_URL}/conceptclass`;
export const DRUG = `${BASE_URL}/drug`;
export const RELATIONSHIP = `${BASE_URL}/relationship_type`;
export const PRIVILEGE = `${BASE_URL}/privilege`;
export const VISIT_TYPE = `${BASE_URL}/visit_type`;
export const PERSON_ATTRIBUTE_TYPE = `${BASE_URL}/person_attribute_type`;
